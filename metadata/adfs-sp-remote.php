<?php

$metadata['https://www.degreed.com'] = array (
    'SingleLogoutService' => 
    array (
      0 => 
      array (
        'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        'Location' => 'https://evademotest.azurewebsites.net/module.php/saml/sp/saml2-logout.php/default-sp',
      ),
    ),
    'AssertionConsumerService' => 
    array (
      0 => 
      array (
        'index' => 0,
        'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        'Location' => 'https://evademotest.azurewebsites.net/module.php/saml/sp/saml2-acs.php/default-sp',
      ),
      1 => 
      array (
        'index' => 1,
        'Binding' => 'urn:oasis:names:tc:SAML:1.0:profiles:browser-post',
        'Location' => 'https://evademotest.azurewebsites.net/module.php/saml/sp/saml1-acs.php/default-sp',
      ),
      2 => 
      array (
        'index' => 2,
        'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
        'Location' => 'https://evademotest.azurewebsites.net/module.php/saml/sp/saml2-acs.php/default-sp',
      ),
      3 => 
      array (
        'index' => 3,
        'Binding' => 'urn:oasis:names:tc:SAML:1.0:profiles:artifact-01',
        'Location' => 'https://evademotest.azurewebsites.net/module.php/saml/sp/saml1-acs.php/default-sp/artifact',
      ),
    ),
    'certData' => 'MIIFcTCCA9mgAwIBAgIJAI/tPCA87IMBMA0GCSqGSIb3DQEBBQUAMIGBMQswCQYDVQQGEwJJbjESMBAGA1UECBMJUmFqYXN0aGFuMQ8wDQYDVQQHEwZqYWlwdXIxEDAOBgNVBAoTB2R1cmFwaWQxCzAJBgNVBAsTAklUMQ4wDAYDVQQDEwVHb3BhbDEeMBwGCSqGSIb3DQEJARYPZ29wYWxAZ21haWwuY29tMB4XDTIwMDIxMDE0MDc0OVoXDTMwMDIwOTE0MDc0OVowgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb20wggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQDdJckBBAW5Wt6hP18HOIt+FiWVwlv2IWByX0M59cbvhY5nvxaZjjpVr2RXZhonLLCpEDoFp4WAG2Ms4nK3HNBU6Pnp4iqaqw1nFHNvp+OthePlxeoSdzG/ziSU/WnPAU5C+mt8IZ06yBP0rkFf/D6YmTkcaTxGXDI1by+4SNdIsMVMK+wjTz5MFY74waNxdvgpT4PNBNkB0F1TvIQEOAFLfSN5jF8pll+sZhbKdPc6jHzSDh4T7r8C5sQ5AYZegyJZS/BI/9ETKk9HgjpMupxUFTsCh4zq5Sn9ow9Ebmk6fT7uT2T3DR+8p8uFtJIa+dSYPAldIDHMgr/mIfs15BeqTrQeCbGOjjP/dciC7zhYgAIjhSSoOfy+F57Uu6QeT+9LOThlliTwfzTQhujE9kVlmtndnM3T4+uXo/II14rav0MxqqNldRb0RRN+1j5gO4MDbyGnpVuj1QYwa2G0CO8Ue1y4v+wy0OprXCTeHxLclJBvb8/XfSHECJa1JkdeWM0CAwEAAaOB6TCB5jAdBgNVHQ4EFgQUpk2IgMuOJfTtIqbK5b5j4QS+t2wwgbYGA1UdIwSBrjCBq4AUpk2IgMuOJfTtIqbK5b5j4QS+t2yhgYekgYQwgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb22CCQCP7TwgPOyDATAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBgQDOilS3AAa2ub+eM0pLwm2kGEWuNAYLw8kfs2KJKzlj/oLdiFbBSMtlLT4KrOSelwETmg82Fa358/sxOR3o2N6/9CvejOeVnZ2mflSC8z7SUg5wTdV5uwcvpDd/K0qDDQCWF7Ejh+lj8cvPmm/tT/L8zgNgSYIdRcq0XRF1YMfasm/nhW2wYT92caO450okiy4uY3uY7zauZyNfv12cJPA9U0rlGn4d7LS/Z+/CnlRxMT+NYToEQHcr657Ft20Q/mMZQw8fNAmp4dWtgjCfi7iIfsX/QHka5WQMnoZ1GHwN5s/ODBnUM3p8gNjpqghy1IaOSNRyRXH9jWUIE1bnYFlOZRQnl8FQ9bTcIw7pUo5ZX875VUFtAhWilun72jDJJMAoQZzPM89UAQ0wKPtd411DqtzhPlQV+IBKCZneqsazif7cw11kwijKYurtRNMAAN80bkpnH8GiQQ0rR7FP0NFaqsnVRBJpd8kWoGoreuvl6PsJe7MqtbLJSLjHAYjb1a0=',
  );
