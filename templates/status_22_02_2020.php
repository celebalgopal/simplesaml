<?php
if (array_key_exists('header', $this->data)) {
    if ($this->getTranslator()->getTag($this->data['header']) !== null) {
        $this->data['header'] = $this->t($this->data['header']);
    }
}

$this->includeAtTemplateBase('includes/header.php');
$this->includeAtTemplateBase('includes/attributes.php');
?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $( document ).ready(function() { 
            return false;
            $('#submitForm').attr('action','https://degreed.com/?orgsso=tatacommunication');
            $('#submitForm').attr('method','post');
            $('#submitForm').attr('name','SAMLResponse');
            $('#submitForm').submit();
        })
</script>
    <h2><?php if (isset($this->data['header'])) {
            echo $this->data['header'];
        } else {
            echo $this->t('{status:some_error_occurred}');
        } ?></h2>

    <p><?php echo $this->t('{status:intro}'); ?></p>

<?php
if (isset($this->data['remaining'])) {
    echo '<p>'.$this->t('{status:validfor}', ['%SECONDS%' => $this->data['remaining']]).'</p>';
}

if (isset($this->data['sessionsize'])) {
    echo '<p>'.$this->t('{status:sessionsize}', ['%SIZE%' => $this->data['sessionsize']]).'</p>';
}
?>
    <h2><?php echo $this->t('{status:attributes_header}'); ?></h2>
<?php
$attributes = $this->data['attributes'];
echo present_attributes($this, $attributes, '');

$nameid = $this->data['nameid'];
if ($nameid !== false) {
    /** @var \SAML2\XML\saml\NameID $nameid */
    echo "<h2>".$this->t('{status:subject_header}')."</h2>";
    if ($nameid->getValue() === null) {
        $list = ["NameID" => [$this->t('{status:subject_notset}')]];
        echo "<p>NameID: <span class=\"notset\">".$this->t('{status:subject_notset}')."</span></p>";
    } else {
        $list = [
            "NameId" => [$nameid->getValue()],
        ];
        if ($nameid->getFormat() !== null) {
            $list[$this->t('{status:subject_format}')] = [$nameid->getFormat()];
        }
        if ($nameid->getNameQualifier() !== null) {
            $list['NameQualifier'] = [$nameid->getNameQualifier()];
        }
        if ($nameid->getSPNameQualifier() !== null) {
            $list['SPNameQualifier'] = [$nameid->getSPNameQualifier()];
        }
        if ($nameid->getSPProvidedID() !== null) {
            $list['SPProvidedID'] = [$nameid->getSPProvidedID()];
        }
    }
    echo present_attributes($this, $list, '');
}

$authData = $this->data['authData'];
$authData['ATTRIBUTES_MAP'] = $authData['Attributes'];
$authData['Attribute'] = $authData['Attributes'];
$authData['X509Certificate'] = "MIIFcTCCA9mgAwIBAgIJAI/tPCA87IMBMA0GCSqGSIb3DQEBBQUAMIGBMQswCQYDVQQGEwJJbjESMBAGA1UECBMJUmFqYXN0aGFuMQ8wDQYDVQQHEwZqYWlwdXIxEDAOBgNVBAoTB2R1cmFwaWQxCzAJBgNVBAsTAklUMQ4wDAYDVQQDEwVHb3BhbDEeMBwGCSqGSIb3DQEJARYPZ29wYWxAZ21haWwuY29tMB4XDTIwMDIxMDE0MDc0OVoXDTMwMDIwOTE0MDc0OVowgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb20wggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQDdJckBBAW5Wt6hP18HOIt+FiWVwlv2IWByX0M59cbvhY5nvxaZjjpVr2RXZhonLLCpEDoFp4WAG2Ms4nK3HNBU6Pnp4iqaqw1nFHNvp+OthePlxeoSdzG/ziSU/WnPAU5C+mt8IZ06yBP0rkFf/D6YmTkcaTxGXDI1by+4SNdIsMVMK+wjTz5MFY74waNxdvgpT4PNBNkB0F1TvIQEOAFLfSN5jF8pll+sZhbKdPc6jHzSDh4T7r8C5sQ5AYZegyJZS/BI/9ETKk9HgjpMupxUFTsCh4zq5Sn9ow9Ebmk6fT7uT2T3DR+8p8uFtJIa+dSYPAldIDHMgr/mIfs15BeqTrQeCbGOjjP/dciC7zhYgAIjhSSoOfy+F57Uu6QeT+9LOThlliTwfzTQhujE9kVlmtndnM3T4+uXo/II14rav0MxqqNldRb0RRN+1j5gO4MDbyGnpVuj1QYwa2G0CO8Ue1y4v+wy0OprXCTeHxLclJBvb8/XfSHECJa1JkdeWM0CAwEAAaOB6TCB5jAdBgNVHQ4EFgQUpk2IgMuOJfTtIqbK5b5j4QS+t2wwgbYGA1UdIwSBrjCBq4AUpk2IgMuOJfTtIqbK5b5j4QS+t2yhgYekgYQwgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb22CCQCP7TwgPOyDATAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBgQDOilS3AAa2ub+eM0pLwm2kGEWuNAYLw8kfs2KJKzlj/oLdiFbBSMtlLT4KrOSelwETmg82Fa358/sxOR3o2N6/9CvejOeVnZ2mflSC8z7SUg5wTdV5uwcvpDd/K0qDDQCWF7Ejh+lj8cvPmm/tT/L8zgNgSYIdRcq0XRF1YMfasm/nhW2wYT92caO450okiy4uY3uY7zauZyNfv12cJPA9U0rlGn4d7LS/Z+/CnlRxMT+NYToEQHcr657Ft20Q/mMZQw8fNAmp4dWtgjCfi7iIfsX/QHka5WQMnoZ1GHwN5s/ODBnUM3p8gNjpqghy1IaOSNRyRXH9jWUIE1bnYFlOZRQnl8FQ9bTcIw7pUo5ZX875VUFtAhWilun72jDJJMAoQZzPM89UAQ0wKPtd411DqtzhPlQV+IBKCZneqsazif7cw11kwijKYurtRNMAAN80bkpnH8GiQQ0rR7FP0NFaqsnVRBJpd8kWoGoreuvl6PsJe7MqtbLJSLjHAYjb1a0=";
if (!empty($authData)) {
    echo "<h2>".$this->t('{status:authData_header}')."</h2>";
    echo '<details><summary>'.$this->t('{status:authData_summary}').'</summary>'; 
    echo '<pre>'.htmlspecialchars(json_encode($authData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)).'</pre>';
    echo '</details>';
}
if (isset($this->data['logout'])) {
    echo '<h2>'.$this->t('{status:logout}').'</h2>';
    echo '<p>'.$this->data['logout'].'</p>';
}

if (isset($this->data['logouturl'])) {
    echo '<a href="'.htmlspecialchars($this->data['logouturl']).'">'.$this->t('{status:logout}').'</a>';
}

$this->includeAtTemplateBase('includes/footer.php'); ?>
<form id="submitForm">
    <input type="hidden" name="SAMLResponse" value='<?php echo json_encode($authData); ?>'>
</form>
