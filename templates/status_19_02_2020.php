<?php
if (array_key_exists('header', $this->data)) {
    if ($this->getTranslator()->getTag($this->data['header']) !== null) {
        $this->data['header'] = $this->t($this->data['header']);
    }
}

$this->includeAtTemplateBase('includes/header.php');
$this->includeAtTemplateBase('includes/attributes.php');
?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $( document ).ready(function() {
            //$.post("https://evesaml.azurewebsites.net/saml2_auth/acs/",
            // $.post("http://durapid.com/",
            //     {
            //     SAMLResponse: "GOPAL",
            //     },
            //     function(data,status){
            //     alert("Data: " + data + "\nStatus: " + status);
            // });
            //alert('I M HERER>>>>>>>');
            //return false;
            $('#submitForm').attr('action','http://13.71.7.157:8000/saml2_auth/acs/');
            $('#submitForm').attr('method','post');
            $('#submitForm').attr('name','SAMLResponse');
            $('#submitForm').submit();
        })
</script>
    <h2><?php if (isset($this->data['header'])) {
            echo $this->data['header'];
        } else {
            echo $this->t('{status:some_error_occurred}');
        } ?></h2>

    <p><?php echo $this->t('{status:intro}'); ?></p>

<?php
if (isset($this->data['remaining'])) {
    echo '<p>'.$this->t('{status:validfor}', ['%SECONDS%' => $this->data['remaining']]).'</p>';
}

if (isset($this->data['sessionsize'])) {
    echo '<p>'.$this->t('{status:sessionsize}', ['%SIZE%' => $this->data['sessionsize']]).'</p>';
}
?>
    <h2><?php echo $this->t('{status:attributes_header}'); ?></h2>
<?php
$attributes = $this->data['attributes'];
echo present_attributes($this, $attributes, '');

$nameid = $this->data['nameid'];
if ($nameid !== false) {
    /** @var \SAML2\XML\saml\NameID $nameid */
    echo "<h2>".$this->t('{status:subject_header}')."</h2>";
    if ($nameid->getValue() === null) {
        $list = ["NameID" => [$this->t('{status:subject_notset}')]];
        echo "<p>NameID: <span class=\"notset\">".$this->t('{status:subject_notset}')."</span></p>";
    } else {
        $list = [
            "NameId" => [$nameid->getValue()],
        ];
        if ($nameid->getFormat() !== null) {
            $list[$this->t('{status:subject_format}')] = [$nameid->getFormat()];
        }
        if ($nameid->getNameQualifier() !== null) {
            $list['NameQualifier'] = [$nameid->getNameQualifier()];
        }
        if ($nameid->getSPNameQualifier() !== null) {
            $list['SPNameQualifier'] = [$nameid->getSPNameQualifier()];
        }
        if ($nameid->getSPProvidedID() !== null) {
            $list['SPProvidedID'] = [$nameid->getSPProvidedID()];
        }
    }
    echo present_attributes($this, $list, '');
}

$authData = $this->data['authData'];
$authData['ATTRIBUTES_MAP'] = $authData['Attributes'];
if (!empty($authData)) {
    echo "<h2>".$this->t('{status:authData_header}')."</h2>";
    echo '<details><summary>'.$this->t('{status:authData_summary}').'</summary>'; 
    echo '<pre>'.htmlspecialchars(json_encode($authData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)).'</pre>';
    echo '</details>';
}
if (isset($this->data['logout'])) {
    echo '<h2>'.$this->t('{status:logout}').'</h2>';
    echo '<p>'.$this->data['logout'].'</p>';
}

if (isset($this->data['logouturl'])) {
    echo '<a href="'.htmlspecialchars($this->data['logouturl']).'">'.$this->t('{status:logout}').'</a>';
}

$this->includeAtTemplateBase('includes/footer.php'); ?>
<form id="submitForm">
    <input type="hidden" name="SAMLResponse" value='<?php echo json_encode($authData); ?>'>
</form>
