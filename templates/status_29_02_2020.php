<?php
if (array_key_exists('header', $this->data)) {
    if ($this->getTranslator()->getTag($this->data['header']) !== null) {
        $this->data['header'] = $this->t($this->data['header']);
    }
}

$this->includeAtTemplateBase('includes/header.php');
$this->includeAtTemplateBase('includes/attributes.php');
?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $( document ).ready(function() { 
            return false;
            $('#submitForm').attr('action','https://degreed.com/SAML/AssertionConsumerService');
            $('#submitForm').attr('method','post');
            $('#submitForm').attr('name','SAMLResponse');
            $('#submitForm').submit();
        })
</script>
    <h2><?php if (isset($this->data['header'])) {
            echo $this->data['header'];
        } else {
            echo $this->t('{status:some_error_occurred}');
        } ?></h2>

    <p><?php echo $this->t('{status:intro}'); ?></p>

<?php
if (isset($this->data['remaining'])) {
    echo '<p>'.$this->t('{status:validfor}', ['%SECONDS%' => $this->data['remaining']]).'</p>';
}

if (isset($this->data['sessionsize'])) {
    echo '<p>'.$this->t('{status:sessionsize}', ['%SIZE%' => $this->data['sessionsize']]).'</p>';
}
?>
    <h2><?php echo $this->t('{status:attributes_header}'); ?></h2>
<?php
$attributes = $this->data['attributes'];
echo present_attributes($this, $attributes, '');

$nameid = $this->data['nameid'];
if ($nameid !== false) {
    /** @var \SAML2\XML\saml\NameID $nameid */
    echo "<h2>".$this->t('{status:subject_header}')."</h2>";
    if ($nameid->getValue() === null) {
        $list = ["NameID" => [$this->t('{status:subject_notset}')]];
        echo "<p>NameID: <span class=\"notset\">".$this->t('{status:subject_notset}')."</span></p>";
    } else {
        $list = [
            "NameId" => [$nameid->getValue()],
        ];
        if ($nameid->getFormat() !== null) {
            $list[$this->t('{status:subject_format}')] = [$nameid->getFormat()];
        }
        if ($nameid->getNameQualifier() !== null) {
            $list['NameQualifier'] = [$nameid->getNameQualifier()];
        }
        if ($nameid->getSPNameQualifier() !== null) {
            $list['SPNameQualifier'] = [$nameid->getSPNameQualifier()];
        }
        if ($nameid->getSPProvidedID() !== null) {
            $list['SPProvidedID'] = [$nameid->getSPProvidedID()];
        }
    }
    echo present_attributes($this, $list, '');
}

$authData = $this->data['authData'];
$authData['ATTRIBUTES_MAP'] = $authData['Attributes'];
$authData['Attribute'] = $authData['Attributes'];
$authData['X509Certificate'] = "MIIFcTCCA9mgAwIBAgIJAI/tPCA87IMBMA0GCSqGSIb3DQEBBQUAMIGBMQswCQYDVQQGEwJJbjESMBAGA1UECBMJUmFqYXN0aGFuMQ8wDQYDVQQHEwZqYWlwdXIxEDAOBgNVBAoTB2R1cmFwaWQxCzAJBgNVBAsTAklUMQ4wDAYDVQQDEwVHb3BhbDEeMBwGCSqGSIb3DQEJARYPZ29wYWxAZ21haWwuY29tMB4XDTIwMDIxMDE0MDc0OVoXDTMwMDIwOTE0MDc0OVowgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb20wggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQDdJckBBAW5Wt6hP18HOIt+FiWVwlv2IWByX0M59cbvhY5nvxaZjjpVr2RXZhonLLCpEDoFp4WAG2Ms4nK3HNBU6Pnp4iqaqw1nFHNvp+OthePlxeoSdzG/ziSU/WnPAU5C+mt8IZ06yBP0rkFf/D6YmTkcaTxGXDI1by+4SNdIsMVMK+wjTz5MFY74waNxdvgpT4PNBNkB0F1TvIQEOAFLfSN5jF8pll+sZhbKdPc6jHzSDh4T7r8C5sQ5AYZegyJZS/BI/9ETKk9HgjpMupxUFTsCh4zq5Sn9ow9Ebmk6fT7uT2T3DR+8p8uFtJIa+dSYPAldIDHMgr/mIfs15BeqTrQeCbGOjjP/dciC7zhYgAIjhSSoOfy+F57Uu6QeT+9LOThlliTwfzTQhujE9kVlmtndnM3T4+uXo/II14rav0MxqqNldRb0RRN+1j5gO4MDbyGnpVuj1QYwa2G0CO8Ue1y4v+wy0OprXCTeHxLclJBvb8/XfSHECJa1JkdeWM0CAwEAAaOB6TCB5jAdBgNVHQ4EFgQUpk2IgMuOJfTtIqbK5b5j4QS+t2wwgbYGA1UdIwSBrjCBq4AUpk2IgMuOJfTtIqbK5b5j4QS+t2yhgYekgYQwgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb22CCQCP7TwgPOyDATAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBgQDOilS3AAa2ub+eM0pLwm2kGEWuNAYLw8kfs2KJKzlj/oLdiFbBSMtlLT4KrOSelwETmg82Fa358/sxOR3o2N6/9CvejOeVnZ2mflSC8z7SUg5wTdV5uwcvpDd/K0qDDQCWF7Ejh+lj8cvPmm/tT/L8zgNgSYIdRcq0XRF1YMfasm/nhW2wYT92caO450okiy4uY3uY7zauZyNfv12cJPA9U0rlGn4d7LS/Z+/CnlRxMT+NYToEQHcr657Ft20Q/mMZQw8fNAmp4dWtgjCfi7iIfsX/QHka5WQMnoZ1GHwN5s/ODBnUM3p8gNjpqghy1IaOSNRyRXH9jWUIE1bnYFlOZRQnl8FQ9bTcIw7pUo5ZX875VUFtAhWilun72jDJJMAoQZzPM89UAQ0wKPtd411DqtzhPlQV+IBKCZneqsazif7cw11kwijKYurtRNMAAN80bkpnH8GiQQ0rR7FP0NFaqsnVRBJpd8kWoGoreuvl6PsJe7MqtbLJSLjHAYjb1a0=";
if (!empty($authData)) {
    echo "<h2>".$this->t('{status:authData_header}')."</h2>";
    echo '<details><summary>'.$this->t('{status:authData_summary}').'</summary>'; 
    echo '<pre>'.htmlspecialchars(json_encode($authData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)).'</pre>';
    echo '</details>';
}
$xmlDataString = '<?xml version="1.0" encoding="UTF-8"?>
<samlp:Response xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ID="_f6aab476-cc43-42b3-b016-0e010eacf965" Version="2.0" IssueInstant="'.gmdate("Y-m-d\TH:i:s\Z").'" Destination="https://degreed.com/SAML/AssertionConsumerService">
   <saml:Issuer xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">"https://evademotest.azurewebsites.net/simplesamlphp/www/"</saml:Issuer>
   <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
      <SignedInfo>
         <CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />
         <SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256" />
         <Reference URI="#_d43a2c1326e005e723f012afd095dfc355cd8e779fb82589d1c5f80440e37fe3">
            <Transforms>
               <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />
               <Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
                  <InclusiveNamespaces xmlns="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList="#default samlp saml ds xs xsi" />
               </Transform>
            </Transforms>
            <DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256" />
            <DigestValue>6XFqnx489Otode8OoWKL+lE0zncSazhgXhndyJGkLWw=</DigestValue>
         </Reference>
      </SignedInfo>
      <SignatureValue>LFQWEs667wwL5MoY1lzyaB6UVW7wFrZT0n6uML1QywBjnJLTv+ccF623p2TNdHiIZV1IffPY5eXK5p1TkzcBHxvmjYb2/Pt8bScrBnT2wsaVvm26LAf85BALoCwNYYAkebuICwvbZ0P40yHnX8ESD29F7t/uGwPqB4Y+S4G4SCvMRv+HpEHydlFaMRNuSDB/jXgESpDZkBYuD83BlhvAzRxjepgklk3WxRpUBc1vHFljas2utqQhdZLFsjkWcHgF8+g1VwnECeP/TzJE0Ra4Sdbt80JHbVB935caQgC+JCsi9mdeiqZ1gjnH+tt5BldZhkTxKAzKslXjpG28WQyysGS1LW1hRFfwKzVg0rUQh2YXkgRynp72wParDUva1HybjBPcA853StwqA7v7PRuk0dfFSjUTdMFXDbxRcc3vBUtmKlKdmECSCgtqRWKHpTXzRgriUL9GY+61f6PBS8oRbtmlMaIozOqj+bJWgAa5/42SpOsxNVuX6lJoYgEGNJ0B</SignatureValue>
      <KeyInfo>
         <X509Data>
            <X509Certificate>MIIFcTCCA9mgAwIBAgIJAI/tPCA87IMBMA0GCSqGSIb3DQEBBQUAMIGBMQswCQYDVQQGEwJJbjESMBAGA1UECBMJUmFqYXN0aGFuMQ8wDQYDVQQHEwZqYWlwdXIxEDAOBgNVBAoTB2R1cmFwaWQxCzAJBgNVBAsTAklUMQ4wDAYDVQQDEwVHb3BhbDEeMBwGCSqGSIb3DQEJARYPZ29wYWxAZ21haWwuY29tMB4XDTIwMDIxMDE0MDc0OVoXDTMwMDIwOTE0MDc0OVowgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb20wggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQDdJckBBAW5Wt6hP18HOIt+FiWVwlv2IWByX0M59cbvhY5nvxaZjjpVr2RXZhonLLCpEDoFp4WAG2Ms4nK3HNBU6Pnp4iqaqw1nFHNvp+OthePlxeoSdzG/ziSU/WnPAU5C+mt8IZ06yBP0rkFf/D6YmTkcaTxGXDI1by+4SNdIsMVMK+wjTz5MFY74waNxdvgpT4PNBNkB0F1TvIQEOAFLfSN5jF8pll+sZhbKdPc6jHzSDh4T7r8C5sQ5AYZegyJZS/BI/9ETKk9HgjpMupxUFTsCh4zq5Sn9ow9Ebmk6fT7uT2T3DR+8p8uFtJIa+dSYPAldIDHMgr/mIfs15BeqTrQeCbGOjjP/dciC7zhYgAIjhSSoOfy+F57Uu6QeT+9LOThlliTwfzTQhujE9kVlmtndnM3T4+uXo/II14rav0MxqqNldRb0RRN+1j5gO4MDbyGnpVuj1QYwa2G0CO8Ue1y4v+wy0OprXCTeHxLclJBvb8/XfSHECJa1JkdeWM0CAwEAAaOB6TCB5jAdBgNVHQ4EFgQUpk2IgMuOJfTtIqbK5b5j4QS+t2wwgbYGA1UdIwSBrjCBq4AUpk2IgMuOJfTtIqbK5b5j4QS+t2yhgYekgYQwgYExCzAJBgNVBAYTAkluMRIwEAYDVQQIEwlSYWphc3RoYW4xDzANBgNVBAcTBmphaXB1cjEQMA4GA1UEChMHZHVyYXBpZDELMAkGA1UECxMCSVQxDjAMBgNVBAMTBUdvcGFsMR4wHAYJKoZIhvcNAQkBFg9nb3BhbEBnbWFpbC5jb22CCQCP7TwgPOyDATAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBgQDOilS3AAa2ub+eM0pLwm2kGEWuNAYLw8kfs2KJKzlj/oLdiFbBSMtlLT4KrOSelwETmg82Fa358/sxOR3o2N6/9CvejOeVnZ2mflSC8z7SUg5wTdV5uwcvpDd/K0qDDQCWF7Ejh+lj8cvPmm/tT/L8zgNgSYIdRcq0XRF1YMfasm/nhW2wYT92caO450okiy4uY3uY7zauZyNfv12cJPA9U0rlGn4d7LS/Z+/CnlRxMT+NYToEQHcr657Ft20Q/mMZQw8fNAmp4dWtgjCfi7iIfsX/QHka5WQMnoZ1GHwN5s/ODBnUM3p8gNjpqghy1IaOSNRyRXH9jWUIE1bnYFlOZRQnl8FQ9bTcIw7pUo5ZX875VUFtAhWilun72jDJJMAoQZzPM89UAQ0wKPtd411DqtzhPlQV+IBKCZneqsazif7cw11kwijKYurtRNMAAN80bkpnH8GiQQ0rR7FP0NFaqsnVRBJpd8kWoGoreuvl6PsJe7MqtbLJSLjHAYjb1a0=</X509Certificate>
         </X509Data>
      </KeyInfo>
   </Signature>
   <samlp:Status>
      <samlp:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:Success" />
   </samlp:Status>
   <saml:Assertion xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" Version="2.0" ID="_bdc50f6c-ac80-476b-8cb6-f0bd90cbd3e7" IssueInstant="'.gmdate("Y-m-d\TH:i:s\Z").'">
      <saml:Issuer>https://evademotest.azurewebsites.net/simplesamlphp/www/</saml:Issuer>
      <saml:Subject>
         <saml:NameID format="urn:oasis:names:tc:saml:1.1:nameid-format:unspecified">'.$authData['Attributes']['Email'][0].'</saml:NameID>
         <saml:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
            <saml:SubjectConfirmationData NotOnOrAfter="'.gmdate("Y-m-d\TH:i:s\Z").'" Recipient="https://degreed.com/DegreedSP" />
         </saml:SubjectConfirmation>
      </saml:Subject>
      <saml:Conditions NotBefore="'.gmdate("Y-m-d\TH:i:s\Z").'" NotOnOrAfter="'.gmdate("Y-m-d\TH:i:s\Z").'">
         <saml:AudienceRestriction>
            <saml:Audience />
         </saml:AudienceRestriction>
      </saml:Conditions>
      <saml:AuthnStatement AuthnInstant="'.gmdate("Y-m-d\TH:i:s\Z").'" SessionIndex="A77af0ab8">
         <saml:AuthnContext>
            <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml:AuthnContextClassRef>
         </saml:AuthnContext>
      </saml:AuthnStatement>
      <saml:AttributeStatement>
         <saml:Attribute Name="FirstName" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:basic">
            <saml:AttributeValue>'.$authData['Attributes']['FirstName'][0].'</saml:AttributeValue>
         </saml:Attribute>
         <saml:Attribute Name="LastName" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:basic">
            <saml:AttributeValue>'.$authData['Attributes']['LastName'][0].'</saml:AttributeValue>
         </saml:Attribute>
         <saml:Attribute Name="EmployeeNumer" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
            <saml:AttributeValue>'.$authData['Attributes']['UserId'][0].'</saml:AttributeValue>
         </saml:Attribute>
         <saml:Attribute Name="Email" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:basic">
            <saml:AttributeValue>'.$authData['Attributes']['Email'][0].'</saml:AttributeValue>
         </saml:Attribute>
      </saml:AttributeStatement>
   </saml:Assertion>
</samlp:Response>';
?>

    
    <p>SAML XML DATA</p>
    <div class="metadatabox">
        
        <pre id="xmlmetadata"><?php  echo htmlspecialchars($xmlDataString); ?></pre>
    </div>
<?php
if (isset($this->data['logout'])) {
    echo '<h2>'.$this->t('{status:logout}').'</h2>';
    echo '<p>'.$this->data['logout'].'</p>';
}

if (isset($this->data['logouturl'])) {
    echo '<a href="'.htmlspecialchars($this->data['logouturl']).'">'.$this->t('{status:logout}').'</a>';
}

$this->includeAtTemplateBase('includes/footer.php'); ?>
<form id="submitForm">
    <!-- <input type="hidden" name="SAMLResponse" value='<?php echo json_encode($authData); ?>'> -->
    <input type="hidden" name="SAMLResponse" value='<?php echo htmlspecialchars($xmlDataString); ?>'>
</form>
